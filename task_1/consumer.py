import pika
import psycopg2
import time

time.sleep(20)
connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit'))
conn = psycopg2.connect("dbname='postgres' user='postgres' host='database' password='251649' port='5432'")
channel = connection.channel()
cur = conn.cursor()
cur.execute("DROP TABLE IF EXISTS messages;")
cur.execute("CREATE TABLE messages (id SERIAL PRIMARY KEY, msg CHAR(256));")
print("ok")

channel.queue_declare(queue='Queue')

def callback(ch, method, properties, body):
    cur.execute("INSERT INTO messages (msg) VALUES ('%s')" %body.decode('utf-8'))

channel.basic_consume(callback, queue='Queue', no_ack=True)

channel.start_consuming()
