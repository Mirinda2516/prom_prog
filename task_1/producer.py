import pika
import sys
import time

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit'))
channel = connection.channel()
channel.queue_declare(queue='Queue')
while True:
    line = input("Please enter something: ")
    channel.basic_publish(exchange='', routing_key='Queue', body=line)
    print("Sent message")
connection.close()
